#Blinks Client#

**Blinks client** allows you to make some request over the Blinks API server.
 >Learn about it on the [french wiki][1].
###YOU can help###
You can :

* Fork it
* Help translating
* Follow [@blinksyourweb][2]

[1]: https://bitbucket.org/david_lambourdiere/blinks-client/wiki/Home
[2]: https://twitter.com/BlinksYourWeb