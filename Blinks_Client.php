<?php

/**
* Blinks_Client class
* @author predatueur kingroyal.m@gmail.com
* @link https://twitter.com/predatueur
* @version 0.9
* @package Blinks_Client
*/

class Blinks_Client
{
	
	const BLINKS_SERVER = 'http://blinks.olympe.in';

	/**
	* @var string
	*/
	public $status;

	/**
	* @var string
	*/
	public $error;

	/**
	* @var string
	*/
	public $dev_key;

	public function __construct($dev_key)
	{
		$this->dev_key = $dev_key;
	}

	/**
	* joinParams function
	*
	* @access public
	* @return string
	*/
	public function joinParams()
	{
		$params = array('dev_key',$this->dev_key);
		return '?'.join('=',$params);
	}

	/**
	* Make a request too the API server
	* 
	* @access private
	* @param string $method The method you want to use
	* @param string $url The URL you want to access
	* @param array 	$data The data you want to transfer for a POST request
	* @return string JSON response
	* @return null
	*/
	private function request($method, $url, $data= false)
	{
		$params = $this->joinParams();
		$ch = curl_init(self::BLINKS_SERVER . '/api' .$url . $params);
		if ($method == 'post') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$response = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response);
		if($response){
			$this->status = $response->status;
			if ($response->status == "error"){
				$this->error = $response->content;
				return $this->error;
			}	

			return json_decode($response->content, true);
		}
		else{
			$error = 'The client get reach the ressource you asked';
			$this->status = 'error';
			$this->error = $error;
			return $error;
		}
	}

	/**
	* Make a GET request
	*
	* @param string $url The URL you want to access
	* @see request()
	* @return string JSON response
	* @return null
	*/
	public function get($url)
	{
		return $this->request('get', $url);
	}

	/**
	* Make a POST request
	*
	* @param string $url The URL you want to access
	* @param array 	$data The data you want to transfer for a POST request
	* @see request()
	* @return string JSON response
	* @return null
	*/
	public function post($url, $data=false)
	{
		return $this->request('post', $url, $data);
	}

}